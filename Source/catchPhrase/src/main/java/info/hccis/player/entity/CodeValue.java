/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author Kyle
 */
@Entity
@Table(name = "codevalue")

public class CodeValue implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codeValueSequence")
    private Integer codeValueSequence;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "englishDescription")
    private String englishDescription;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "englishDescriptionShort")
    private String englishDescriptionShort;
    @Size(max = 100)
    @Column(name = "frenchDescription")
    private String frenchDescription;
    @Size(max = 20)
    @Column(name = "frenchDescriptionShort")
    private String frenchDescriptionShort;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;
    @JoinColumn(name = "codeTypeId", referencedColumnName = "CodeTypeId")
    @ManyToOne(optional = false)
    private CodeType codeTypeId;

    public CodeValue() {
    }

    public CodeValue(Integer codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    public CodeValue(Integer codeValueSequence, String englishDescription, String englishDescriptionShort) {
        this.codeValueSequence = codeValueSequence;
        this.englishDescription = englishDescription;
        this.englishDescriptionShort = englishDescriptionShort;
    }

    public Integer getCodeValueSequence() {
        return codeValueSequence;
    }

    public void setCodeValueSequence(Integer codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    public String getEnglishDescriptionShort() {
        return englishDescriptionShort;
    }

    public void setEnglishDescriptionShort(String englishDescriptionShort) {
        this.englishDescriptionShort = englishDescriptionShort;
    }

    public String getFrenchDescription() {
        return frenchDescription;
    }

    public void setFrenchDescription(String frenchDescription) {
        this.frenchDescription = frenchDescription;
    }

    public String getFrenchDescriptionShort() {
        return frenchDescriptionShort;
    }

    public void setFrenchDescriptionShort(String frenchDescriptionShort) {
        this.frenchDescriptionShort = frenchDescriptionShort;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public CodeType getCodeTypeId() {
        return codeTypeId;
    }

    public void setCodeTypeId(CodeType codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeValueSequence != null ? codeValueSequence.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CodeValue)) {
            return false;
        }
        CodeValue other = (CodeValue) object;
        if ((this.codeValueSequence == null && other.codeValueSequence != null) || (this.codeValueSequence != null && !this.codeValueSequence.equals(other.codeValueSequence))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.player.entity.Codevalue[ codeValueSequence=" + codeValueSequence + " ]";
    }
    
}
