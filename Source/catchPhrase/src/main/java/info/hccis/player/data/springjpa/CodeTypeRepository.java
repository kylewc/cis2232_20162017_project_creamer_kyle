/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.data.springjpa;

import info.hccis.player.entity.CodeType;
import java.util.ArrayList;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Kyle
 */
@Repository
public interface CodeTypeRepository extends CrudRepository<CodeType, Integer> {

    @Query(value="select max(CodeTypeId) from codetype", nativeQuery = true)
    Integer selectMaxId();
    
    //Find by username.
    @Query(value="select * from codetype where createdUserId=?", nativeQuery = true)
    ArrayList<CodeType> findByUsername(String thisUsername);
}

