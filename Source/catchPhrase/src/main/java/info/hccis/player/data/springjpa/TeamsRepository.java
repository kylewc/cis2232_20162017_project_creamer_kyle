/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.data.springjpa;


import info.hccis.player.entity.Teams;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Kyle
     */
@Repository
public interface TeamsRepository extends CrudRepository<Teams, Integer> {
    
    @Query(value="select * from teams where teamName=?", nativeQuery = true)
    Teams findTeamByName(String teamName);

}