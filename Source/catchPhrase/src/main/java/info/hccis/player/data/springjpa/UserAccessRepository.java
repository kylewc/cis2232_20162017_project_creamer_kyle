/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.data.springjpa;

import info.hccis.player.entity.UserAccess;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Kyle
     */
@Repository
public interface UserAccessRepository extends CrudRepository<UserAccess, Integer> {
    
    @Query(value="select max(userAccessId) from useraccess", nativeQuery = true)
    Integer selectMaxId();

    @Query(value="select * from useraccess where username=?", nativeQuery = true)
    UserAccess findByUsername(String thisUsername);
}


