/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.data.springjpa;


import info.hccis.player.entity.GameDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Kyle
     */
@Repository
public interface GameDetailsRepository extends CrudRepository<GameDetails, Integer> {
    
    @Query(value="select max(gameId) from gamedetails", nativeQuery = true)
    Integer selectMaxId();


}
