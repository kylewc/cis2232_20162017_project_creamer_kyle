package info.hccis.player.web;

import info.hccis.player.data.springjpa.CodeTypeRepository;
import info.hccis.player.data.springjpa.CodeValueRepository;
import info.hccis.player.data.springjpa.GameDetailsRepository;
import info.hccis.player.data.springjpa.TeamsRepository;
import info.hccis.player.data.springjpa.UserAccessRepository;
import info.hccis.player.entity.CodeType;
import info.hccis.player.entity.GameDetails;
import info.hccis.player.entity.Teams;
import info.hccis.player.entity.UserAccess;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GameController {

    private ArrayList<String> wordList = null;
    private final UserAccessRepository userAccessRepository;
    private final CodeTypeRepository codeTypeRepository;
    private final CodeValueRepository codeValueRepository;
    private final GameDetailsRepository gameDetailsRepository;
    private final TeamsRepository teamsRepository;

    @Autowired
    public GameController(UserAccessRepository userAccessRepository, CodeTypeRepository codeTypeRepository, CodeValueRepository codeValueRepository, GameDetailsRepository gameDetailsRepository, TeamsRepository teamsRepository) {
        this.userAccessRepository = userAccessRepository;
        this.codeTypeRepository = codeTypeRepository;
        this.codeValueRepository = codeValueRepository;
        this.gameDetailsRepository = gameDetailsRepository;
        this.teamsRepository = teamsRepository;
    }
    
    //When the game starts we check for authentication and proceed to find or create two teams, and populate the word list.
    @RequestMapping("/startGame")
    public String startGame(Model model, @ModelAttribute("codeType") CodeType codeType, HttpServletRequest request, HttpSession session) {
        //Checking authentication.
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }

        //Finding the word list by its passed id.
        codeType = (CodeType) codeTypeRepository.findOne(codeType.getCodeTypeId());
        //Here we will take values from game setup, and create a game.
        Teams team1 = new Teams();
        Teams team2 = new Teams();

        //We attempt to find a team, and if not found create a new team.
        String teamID = (request.getParameter("teamID"));
        if (teamsRepository.findTeamByName(teamID) == null) {
            team1.setTeamName(teamID);
            team1.setWins(0);
            team1.setLosses(0);
            team1.setTotalGames(0);
            teamsRepository.save(team1);
        } else {
            //If found we use the given team.
            team1 = teamsRepository.findTeamByName(teamID);
        }

        //We attempt fo find a team, and if not found create a new team.
        String teamID2 = (request.getParameter("teamID2"));

        if (teamsRepository.findTeamByName(teamID2) == null) {
            team2.setTeamName(teamID2);
            team2.setWins(0);
            team2.setLosses(0);
            team2.setTotalGames(0);
            teamsRepository.save(team2);
        } else {
            team2 = teamsRepository.findTeamByName(teamID2);
        }
        
        //We create a new game and associate the active user with the game.
        GameDetails newGame = new GameDetails();
        UserAccess thisUser = (UserAccess) session.getAttribute("user");
        //We must now create a game that contains both teamIDs, category ID, creating user, updating user, set scores to 0, creating time, and update time.
        newGame.setCategoryID(codeType);
        newGame.setTeamID(team1);
        newGame.setTeamID2(team2);
        newGame.setUserAccessId(thisUser);
        newGame.setUpdatedUserId(thisUser.getUserAccessId());
        newGame.setCreatedDateTime(new Date());
        newGame.setUpdatedDateTime(new Date());
        newGame.setTeamID2Score(0);
        newGame.setTeamIDScore(0);
        gameDetailsRepository.save(newGame);

        newGame.setCurrentTeam(team1.getTeamName());

        //We will write all of the game details to the DB.
        //We will take a word list, shuffle it, and store it for turns.
        wordList = codeValueRepository.findWords(newGame.getCategoryID().getCodeTypeId());
        Collections.shuffle(wordList);

        //We set the given word list for the game.
        newGame.setWordList(wordList);
        //A word list will need to have at least 13 words if no guesses score points or we will need to repeat words.
        //Ideally if there are four accurate guesses per point, we need a minimum of 4*(6(losing team max points)+7(winning team max points)) = 52 words to not repeat words.
        
        
        model.addAttribute("newGame", newGame);

        //Will lead to a page that controls the game, and then submits to itself until the game is over.
        return "catchPhrase/playGame";
    }

    //This method manages the next word button during game play.
    @RequestMapping(value = "/nextWord", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public String nextWord(@RequestBody String jsonString) {
        
        String currentWord = "";
        String currentTeam = "";
        String team1Name = "";
        String team2Name = "";

        //We pull the default values from the jsonString and place them in a hashmap.
        Map<String, String> map = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper();

        try {
            // convert JSON string to Map
            map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>() {
            });
        } catch (IOException ex) {
            Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //We assign the hashmap values to string values for simple modification.
        currentWord = map.get("currentWord");
        currentTeam = map.get("currentTeam");
        team1Name = map.get("team1Name");
        team2Name = map.get("team2Name");

        //We check which team is the current team, and modify the current team
        //to the other team.
        if (currentTeam.equals(team1Name)) {
            currentTeam = team2Name;
        } else if (currentTeam.equals(team2Name)) {
            currentTeam = team1Name;
        }

        //We make a check for an empty word list as we may have gotten here with no words from a prior 'next word' call.
        //If the list is not empty, we remove one word and check if it is empty again as we may have removed the last word.
        //If the last word was removed, we have a default string to let the user know there are no more words.
        //If the word list is not empty we set the new word.
        if (wordList.isEmpty() == false) {
            wordList.remove(0);
            if (wordList.isEmpty()) {
                currentWord = "No more words! Add more! Make one up right now!";
            } else {
                currentWord = wordList.get(0);
            }
        }
        //We place the values back into the hashmap and return a json string to ajax.
        map.put("currentWord", currentWord);
        map.put("currentTeam", currentTeam);
        try {
            jsonString = new ObjectMapper().writeValueAsString(map);
            System.out.println(jsonString);

        } catch (IOException ex) {
            Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Need to write json to an arraylist to use values as needed.
        return jsonString;
    }
    
    //This function will manage turns where the time alloted has run out, and a point must be scored and a new word set.
    @RequestMapping(value = "/nextTurn", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public String nextTurn(@RequestBody String jsonString) {
        String gameId = "";
        String team1Name = "";
        String team2Name = "";
        String currentWord = "";
        String currentTeam = "";
        String team1Score = "";
        String team2Score = "";
        int currentScore = 0;

        //We take all values from the json string and place them into simple data for modification.
        Map<String, String> map = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper();

        try {
            // convert JSON string to Map
            map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>() {
            });
        } catch (IOException ex) {
            Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //{gameId=90, currentWord=Dogs, team1Score=0, team2Score=0, currentTeam=asdf}
        gameId = map.get("gameId");
        team1Name = map.get("team1Name");
        team2Name = map.get("team2Name");
        currentWord = map.get("currentWord");
        currentTeam = map.get("currentTeam");
        team1Score = map.get("team1Score");
        team2Score = map.get("team2Score");
        String winner = "";

        //We find the existing game via its ID.
        GameDetails thisGame = gameDetailsRepository.findOne(Integer.parseInt(gameId));

        //We find both teams by their given ID stored in the game.
        Teams teamOne = thisGame.getTeamID();
        Teams teamTwo = thisGame.getTeamID2();
        
        //We check if the word list is empty, because if so the game must be over.
        if (wordList.isEmpty()) {
            //If team 1 has more points than team 2, team 1 wins and we
            //update scores and statistics.
            if (thisGame.getTeamIDScore() > thisGame.getTeamID2Score()) {
                currentScore = teamOne.getWins();
                currentScore += 1;
                teamOne.setWins(currentScore);
                currentScore = teamOne.getTotalGames();
                currentScore += 1;
                teamOne.setTotalGames(currentScore);
                currentScore = teamTwo.getLosses();
                currentScore += 1;
                teamTwo.setLosses(currentScore);
                currentScore = teamTwo.getTotalGames();
                currentScore += 1;
                teamTwo.setTotalGames(currentScore);

                winner = teamOne.getTeamName();
                
            } 
            //If team 2 has more points than team 1, team 2 wins and we
            //update scores and statistics.
            else if (thisGame.getTeamID2Score() > thisGame.getTeamIDScore()) {
                currentScore = teamTwo.getWins();
                currentScore += 1;
                teamTwo.setWins(currentScore);
                currentScore = teamTwo.getTotalGames();
                currentScore += 1;
                teamTwo.setTotalGames(currentScore);
                currentScore = teamOne.getLosses();
                currentScore += 1;
                teamOne.setLosses(currentScore);
                currentScore = teamOne.getTotalGames();
                currentScore += 1;
                teamOne.setTotalGames(currentScore);

                winner = teamTwo.getTeamName();
                
            } 
            //If there is a tie, we dictate as such and only increment the total games.
            else if (thisGame.getTeamIDScore() == thisGame.getTeamID2Score()) {
                winner = "It's a tie! Be sure to use a large word list.";
                teamTwo.setTotalGames(+1);
                teamOne.setTotalGames(+1);

            }
            //We save both teams as they have been updated.
            teamsRepository.save(teamOne);
            teamsRepository.save(teamTwo);
        } else {

            //Since the time ran out, we give a point to the opposing team who is not the current team, and update the current team.
            if (currentTeam.equals(team1Name)) {
                currentScore = thisGame.getTeamID2Score();
                currentScore += 1;
                currentTeam = team2Name;
                team2Score = Integer.toString(currentScore);
                thisGame.setTeamID2Score(currentScore);
            } else if (currentTeam.equals(team2Name)) {
                currentScore = thisGame.getTeamIDScore();
                currentScore += 1;
                currentTeam = team1Name;
                team1Score = Integer.toString(currentScore);
                thisGame.setTeamIDScore(currentScore);
            }

            gameDetailsRepository.save(thisGame);
            //We remove a word from the word List, and then populate the next word if the list is not empty.
            if (wordList.isEmpty() == false) {
                wordList.remove(0);
                if (wordList.isEmpty()) {
                    currentWord = "No more words! Add more! Make one up right now!";
                } else {
                    currentWord = wordList.get(0);
                }
            }

            //update updatedatetime.
            thisGame.setUpdatedDateTime(new Date());
            //check for a win by determining if a team's score has reached 7.
            //increment games played, win, and loss.
   
            if (thisGame.getTeamIDScore() == 7) {
                currentScore = teamOne.getWins();
                currentScore += 1;
                teamOne.setWins(currentScore);
                currentScore = teamOne.getTotalGames();
                currentScore += 1;
                teamOne.setTotalGames(currentScore);
                currentScore = teamTwo.getLosses();
                currentScore += 1;
                teamTwo.setLosses(currentScore);
                currentScore = teamTwo.getTotalGames();
                currentScore += 1;
                teamTwo.setTotalGames(currentScore);

                winner = teamOne.getTeamName();

            } else if (thisGame.getTeamID2Score() == 7) {
                currentScore = teamTwo.getWins();
                currentScore += 1;
                teamTwo.setWins(currentScore);
                currentScore = teamTwo.getTotalGames();
                currentScore += 1;
                teamTwo.setTotalGames(currentScore);
                currentScore = teamOne.getLosses();
                currentScore += 1;
                teamOne.setLosses(currentScore);
                currentScore = teamOne.getTotalGames();
                currentScore += 1;
                teamOne.setTotalGames(currentScore);

                winner = teamTwo.getTeamName();
            }
            //save teams as they have been updated.
            teamsRepository.save(teamOne);
            teamsRepository.save(teamTwo);

        }
            //We save the game, which contains teams and require them to be saved prior to the game.
            gameDetailsRepository.save(thisGame);

            if (winner.equals("") == false) {
                //If a winner has been declared we include the redirect and winner to trigger the game over call.
                map.put("winner", winner);
                map.put("redirect", "/gameOver");
            }
            //return keys to map and send back to ajax.
            map.put("gameId", gameId);
            map.put("currentWord", currentWord);
            map.put("currentTeam", currentTeam);
            map.put("team1Score", team1Score);
            map.put("team2Score", team2Score);

            try {
                jsonString = new ObjectMapper().writeValueAsString(map);
                System.out.println(jsonString);

            } catch (IOException ex) {
                Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Need to write json to an arraylist to use values as needed.
            return jsonString;
    }
    
    //When a game is over we take all values from the request which has been added
    //via a form on the playgame.js script.
    @RequestMapping("/gameOver")
    public String gameSetup(HttpServletRequest request, HttpSession session, Model model) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        //We take all values, and return them to the gameOver page as model attributes.
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();

            System.out.println(paramName);
            String[] paramValues = request.getParameterValues(paramName);
            for (String paramValue : paramValues) {
                System.out.println(paramValue);
                model.addAttribute(paramName, paramValue);
            }
        }

        return "catchPhrase/gameOver";
    }
    
    //This function will manage setting up a game.  We will declare the given teams and select a word list to be used.
    
    @RequestMapping("/setupGame")
    public String gameSetup(Model model, HttpServletRequest request,
            HttpSession session
    ) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        //We populate two strings for team names.
        String teamID = "";
        String teamID2 = "";
        model.addAttribute("teamID", teamID);
        model.addAttribute("teamID2", teamID2);
        //We will populate the list of categories.
        try {
            UserAccess thisUser = (UserAccess) session.getAttribute("user");
            ArrayList<CodeType> codeTypes = codeTypeRepository.findByUsername(thisUser.getUsername());
            model.addAttribute("codeTypes", codeTypes);
        } catch (Exception ex) {
            System.out.println("error: " + ex);
        }
        return "catchPhrase/gameSetup";
    }
}
