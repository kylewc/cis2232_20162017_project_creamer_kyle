package info.hccis.player.web;

import info.hccis.player.data.springjpa.CodeTypeRepository;
import info.hccis.player.data.springjpa.CodeValueRepository;
import info.hccis.player.data.springjpa.TeamsRepository;
import info.hccis.player.data.springjpa.UserAccessRepository;
import info.hccis.player.entity.CodeType;
import info.hccis.player.entity.CodeValue;
import info.hccis.player.entity.Teams;
import info.hccis.player.entity.UserAccess;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.apache.commons.lang3.StringUtils;

@Controller
public class OtherController {

    private final UserAccessRepository userAccessRepository;
    private final CodeTypeRepository codeTypeRepository;
    private final CodeValueRepository codeValueRepository;
    private final TeamsRepository teamsRepository;

    @Autowired
    public OtherController(UserAccessRepository userAccessRepository, CodeTypeRepository codeTypeRepository, CodeValueRepository codeValueRepository, TeamsRepository teamsRepository) {
        this.userAccessRepository = userAccessRepository;
        this.codeTypeRepository = codeTypeRepository;
        this.codeValueRepository = codeValueRepository;
        this.teamsRepository = teamsRepository;
    }

    //When first visiting, we display the login page.
    @RequestMapping("/")
    public String showHome(Model model) {
        return "catchPhrase/welcome";
    }

    //When a user access any page other than register or welcome, we check if they are currently logged in.
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request, HttpSession session) {

        UserAccess foundUser = new UserAccess();
        String success = "False";
        //Check for access of the user
        String accessLevel = "0";

        //if user is the test user, load test user without verifying password.
        if (user.getUsername() == null || user.getUsername().equals("test")) {
            accessLevel = "1";
            foundUser = userAccessRepository.findByUsername(user.getUsername());
            success = "True";
            //if user is not the test user, check username exists and password matches.
        } else {
            try {
                foundUser = userAccessRepository.findByUsername(user.getUsername());
                if (foundUser.getPassword().equals(user.getPassword())) {
                    accessLevel = "1";
                    success = "True";
                }
            } catch (Exception ex) {
                Logger.getLogger(OtherController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Need to code page to display access log.
        //Here we log the access attempt whether succesful or in error to a log file
//                File file = new File("..\\..\\src\\main\\resurces\\logfiles\\access.txt");
        File file = new File("logfiles/access.txt");
        if (!(file.exists())){
            file.getParentFile().mkdirs();
        }
        
        try (FileWriter fw = new FileWriter(file, true);
                // C:\Users\Kyle\Documents\cis2232_20162017_project_creamer_kyle\Source\catchPhrase\src\main\resources\logfiles\access.txt

                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println("Date:  " + new Date().toString() + " User:  " + user.getUsername() + " Access Level: " + accessLevel + " Success: " + success);
        } catch (IOException e) {
            System.out.println(e);
        }
        //If user does not have access, create error to show on page.
        if (accessLevel.equals("0")) {
            model.addAttribute("invalidLogin", "Your login details were not valid");
            return "/catchPhrase/welcome";
        } else {
            //set a login attribute to be checked on each page
            //user sent to session for user in other methods.
            session.setAttribute("loggedIn", true);
            session.setAttribute("user", foundUser);
            return "catchPhrase/home";
        }

    }

    //We load the register page if visited.
    @RequestMapping("/register")
    public String register(Model model) {

        return "catchPhrase/register";
    }

    // List access is for proof of concept in reading and writing from a deployed file.
    //This function will read the access log and return the existing values.
    @RequestMapping("/listAccess")
    public String listAccess(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request, HttpSession session) {
        File file = new File("logfiles/access.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            ArrayList<String> lines = new ArrayList<>();
            String line;
            try {
                while ((line = br.readLine()) != null) {
                    lines.add(line);
                }
                model.addAttribute("lines", lines);
                return "catchPhrase/listAccess";
            } catch (IOException ex) {
                System.out.println(ex);
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return "catchPhrase/listAccess";
    }

    //When registration is submitted we create a user.
    @RequestMapping("/registerSubmit")
    public String registerSubmit(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request, HttpSession session) {

        System.out.println("BJM in /authenticate, username passed in to controller is:" + user.getUsername());

        String error;
        //If user is attempting to enter a blank password or the password as test we provide an error.
        if (user.getPassword() == null || user.getPassword().equals("test") || user.getPassword().equals("")) {
            error = "A password must be entered, and cannot be test.";
            model.addAttribute("error", error);
            return "catchPhrase/register";
        }
        //If the user attempts to login as test or a blank user, we log them in as test.
        if (user.getUsername() == null || user.getUsername().equals("test") || user.getUsername().equals("")) {
            model.addAttribute("notice", "You are logged in as the test user");
            user.setUsername("test");
            user = userAccessRepository.findByUsername(user.getUsername());
            session.setAttribute("user", user);
            return "catchPhrase/home";
        } else {
            try {
                //find the current max ID of users to specify new ID.
                //Once max ID is found, we set the user password and username, and specify the userTypeCode as 1.
                //We then write the userAccess to the DB using the repository or DAO.

                user.setUserAccessId(userAccessRepository.selectMaxId() + 1);
                user.setUserTypeCode(1);
                user.setDateTimeNow();
                session.setAttribute("loggedIn", true);
                model.addAttribute("user", user);
                session.setAttribute("user", user);
                userAccessRepository.save(user);
            } catch (Exception ex) {
                Logger.getLogger(OtherController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "catchPhrase/home";
        }
    }

    //If team stats are accessed we access all teams, and add them as a model attribute.
    @RequestMapping("/teamStats")
    public String teamStats(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }

        ArrayList<Teams> allTeams = (ArrayList<Teams>) teamsRepository.findAll();
        model.addAttribute("teams", allTeams);

        return "catchPhrase/teamStats";
    }

    //We display the about page when requested.
    @RequestMapping("/about")
    public String about(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        return "catchPhrase/about";
    }

    //When the add word list page is accessed, we create an empty word list and display string for instruction.
    @RequestMapping("/addWordList")
    public String addWordList(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        CodeType codeType = new CodeType();
        model.addAttribute(codeType);
        String wordString = "Enter each word on a new line";
        model.addAttribute("wordString", wordString);
        return "catchPhrase/addWordList";
    }

    //When adding a word list is submitted we create a word list via codetpye and codevalue.
    @RequestMapping("/addWordListSubmit")
    public String addWordListSubmit(Model model, @ModelAttribute("codeType") CodeType codeType, HttpServletRequest request, HttpSession session, String wordString) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        try {
            //We assign values to the codetype.
            UserAccess passedUser = (UserAccess) session.getAttribute("user");
            codeType.setCreatedDateTime(new Date());
            codeType.setCreatedUserId((passedUser.getUsername()));
            codeType.setFrenchDescription("");
            codeType.setUpdatedUserId((passedUser.getUsername()));
            codeType.setUpdatedDateTime(new Date());
            codeTypeRepository.save(codeType);
            //We ensure the word list is safe create an array of words based on new lines.
            wordString = wordString.trim();
            ArrayList<String> wordList = new ArrayList<>(Arrays.asList(wordString.split("\\r?\\n")));
            //For each entry in the word list we add a code value associated with the codetype.
            for (String entry : wordList) {
                CodeValue thisValue = new CodeValue();
                thisValue.setEnglishDescription(entry);
                thisValue.setEnglishDescriptionShort(entry.substring(0, Math.min(entry.length(), 20)));
                thisValue.setCreatedDateTime(new Date());
                thisValue.setUpdatedDateTime(new Date());
                thisValue.setCreatedUserId((passedUser.getUsername()));
                thisValue.setUpdatedUserId((passedUser.getUsername()));
                thisValue.setCodeTypeId(codeType);
                codeValueRepository.save(thisValue);
            }
            //We return a response if successful and return empty values.
            String response = "Word list added successfully";
            model.addAttribute("response", response);
            wordString = "";
            codeType = new CodeType();
            model.addAttribute("codeType", codeType);
            model.addAttribute("wordString", wordString);
        } catch (Exception ex) {
            String response = "There was an error adding your word list.";
            model.addAttribute("response", response);
            model.addAttribute("wordString", wordString);
        }

        return "catchPhrase/addWordList";
    }

    //When selecting a category during editing a word list we access the list, and populate its values to return to the page.
    @RequestMapping("/selectCategory")
    public String selectCategory(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }

        try {
            UserAccess thisUser = (UserAccess) session.getAttribute("user");
            ArrayList<CodeType> codeTypes = codeTypeRepository.findByUsername(thisUser.getUsername());
            model.addAttribute("codeTypes", codeTypes);
            return "catchPhrase/selectCategory";
        } catch (Exception ex) {
            System.out.println("error: " + ex);
        }
        return "catchPhrase/selectCategory";
    }

    //When first accessing the edit word list, we populate the selected word list and return it to the page.
    @RequestMapping("/editWordList")
    public String editWordList(Model model, @ModelAttribute("codeType") CodeType codeType, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        try {
            codeType = codeTypeRepository.findOne(codeType.getCodeTypeId());
            ArrayList<String> wordList = codeValueRepository.findWords(codeType.getCodeTypeId());
            String wordString = StringUtils.join(wordList, '\n');
            model.addAttribute("wordString", wordString);
            model.addAttribute(codeType);

            return "catchPhrase/editWordList";
        } catch (Exception ex) {
            System.out.println("error: " + ex);

        }
        return "catchPhrase/editWordList";
    }

    //When editing a word list has been submitted we need to modify the old values, and update them with the new values.
    @RequestMapping("/editWordListSubmit")
    public String editWordListSubmit(Model model, @ModelAttribute("codeType") CodeType codeType, HttpServletRequest request, HttpSession session, String wordString) {

        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        try {

            //We take the old values to access created user, created date time.
            CodeType oldType = codeTypeRepository.findOne(codeType.getCodeTypeId());

            CodeValue oldValue = codeValueRepository.findOneByCodeTypeId(codeType.getCodeTypeId());
            codeValueRepository.deleteByCodeTypeId(oldType.getCodeTypeId());
            UserAccess passedUser = (UserAccess) session.getAttribute("user");

            //Modifying values for codeType to be saved due to allowing english description to be modified.
            //This requires old values in the oldType to be merged with the english description, and codeTypeID of the new value.
            codeType.setFrenchDescription("");
            codeType.setCreatedUserId(oldType.getCreatedUserId());
            codeType.setCreatedDateTime(oldType.getCreatedDateTime());
            codeType.setUpdatedUserId((passedUser.getUsername()));
            codeType.setUpdatedDateTime(new Date());

            codeTypeRepository.save(codeType);
            //We then add the word list by breaking the list apart to strings just as in the add function, and save their codevalues.
            wordString = wordString.trim();
            ArrayList<String> wordList = new ArrayList<>(Arrays.asList(wordString.split("\\r?\\n")));

            //It must be noted that the created date time, and created user use the old values.
            for (String entry : wordList) {
                CodeValue thisValue = new CodeValue();
                thisValue.setEnglishDescription(entry);
                thisValue.setEnglishDescriptionShort(entry.substring(0, Math.min(entry.length(), 20)));
                thisValue.setCreatedDateTime(oldValue.getCreatedDateTime());
                thisValue.setUpdatedDateTime(new Date());
                thisValue.setCreatedUserId((oldValue.getCreatedUserId()));
                thisValue.setUpdatedUserId((passedUser.getUsername()));
                thisValue.setCodeTypeId(codeType);
                codeValueRepository.save(thisValue);
            }

            String response = "Word list edited successfully";
            model.addAttribute("response", response);

            //Select category function needs to be here to populate select category
            //after a successful edit.  Could likely break up this piece into a
            //function as it is called 4 times in 4 different methods.
            //This method, select category, and delete category, and delete word list.
            try {
                UserAccess thisUser = (UserAccess) session.getAttribute("user");
                ArrayList<CodeType> codeTypes = codeTypeRepository.findByUsername(thisUser.getUsername());
                model.addAttribute("codeTypes", codeTypes);
                return "catchPhrase/selectCategory";
            } catch (Exception ex) {
                System.out.println("error: " + ex);
            }
        } catch (Exception ex) {
            System.out.println("error: " + ex);
            String response = "There was an error editing your word list.";
            model.addAttribute("response", response);
            model.addAttribute("wordString", wordString);
            return "catchPhrase/editWordList";
        }

        return "catchPhrase/selectCategory";
    }

    //When deleting a category we populate the list of word lists to be selected.
    @RequestMapping("/deleteCategory")
    public String deleteWordList(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }

        try {
            UserAccess thisUser = (UserAccess) session.getAttribute("user");
            ArrayList<CodeType> codeTypes = codeTypeRepository.findByUsername(thisUser.getUsername());
            model.addAttribute("codeTypes", codeTypes);
            return "catchPhrase/deleteCategory";
        } catch (Exception ex) {
            System.out.println("error: " + ex);
        }
        return "catchPhrase/deleteCategory";

    }

    //When deleting a word list after selecting the cfategory we find all codevalues for the codetype, and delete them.
    @RequestMapping("/deleteWordList")
    public String deleteWordList(Model model, @ModelAttribute("codeType") CodeType codeType, HttpServletRequest request, HttpSession session) {

        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        try {
            //We find the given code type.
            codeType = codeTypeRepository.findOne(codeType.getCodeTypeId());
            //We delete all code values associated with the code type, and delete the code type.
            codeValueRepository.deleteByCodeTypeId(codeType.getCodeTypeId());
            codeTypeRepository.delete(codeType.getCodeTypeId());

            String response = "Word list deleted successfully";
            model.addAttribute("response", response);

        } catch (Exception ex) {
            System.out.println("error: " + ex);
            String response = "There was an error deleting your word list.";
            model.addAttribute("response", response);
        }
        try {
            //When successful we return the user to the select category page with popualted values.
            UserAccess thisUser = (UserAccess) session.getAttribute("user");
            ArrayList<CodeType> codeTypes = codeTypeRepository.findByUsername(thisUser.getUsername());
            model.addAttribute("codeTypes", codeTypes);
            return "catchPhrase/deleteCategory";
        } catch (Exception ex) {
            System.out.println("error: " + ex);
        }
        return "catchPhrase/deleteCategory";

    }

    //When accessed we display the manage wordlist page.
    @RequestMapping("/manageWordLists")
    public String manageWordLists(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }
        return "catchPhrase/manageWordLists";
    }

    //When logging out, we invalidate the session.
    @RequestMapping("/logout")
    public String logout(Model model, HttpServletRequest request, HttpSession session) {
        session.invalidate();
        return "catchPhrase/welcome";
    }

    //We return the home page when accesed.
    @RequestMapping("/home")
    public String home(Model model, HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("loggedIn") == null) {
            return "catchPhrase/welcome";
        }

        return "catchPhrase/home";
    }
}
