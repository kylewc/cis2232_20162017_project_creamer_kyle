package info.hccis.player.dao;

import info.hccis.player.entity.UserAccess;
import info.hccis.player.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

public class UserAccessDAO {

    public static String getUserTypeCode(String username, String password) {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql;

        try {
            //connect to database
            conn = ConnectionUtils.getConnection();
            
            //create query to get usertypecode
            sql = "SELECT `userTypeCode` FROM `UserAccess` WHERE `username` = '" + username + "' AND `password`= '" + Utility.getHashPassword(password) + "'";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            //get data if statement returns a value
            String userTypeCode = null;
            while (rs.next()) {
                userTypeCode = Integer.toString(rs.getInt("userTypeCode"));
            }

            //return if data found
            if (userTypeCode != null) {
                return userTypeCode;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("ERROR GETTING: User Type Code"
                    + "WHY: " + errorMessage);
        } finally {
            DbUtils.close(ps, conn);
        }
        //if data isn't found return 0
        return "0";
    }
    
        public static synchronized String insert(UserAccess user) throws Exception {
            
        Connection conn = null;
        PreparedStatement ps = null;
        String sql;

        try {
            //connect to database
            conn = ConnectionUtils.getConnection();

            sql = "INSERT INTO `UserAccess`(`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`)"
                    + "VALUES (?,?,?,?,NOW())";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, user.getUserAccessId());
            ps.setString(2, user.getUsername());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getUserTypeCode());
            ps.execute();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        //if data isn't found return 0
        return "0";

    }

    public static synchronized String update(UserAccess user) throws Exception {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql;

        try {
            //connect to database
            String propFileName = "spring.data-access";

            conn = ConnectionUtils.getConnection();

            sql = "UPDATE `UserAccess` SET `username` = '" + user.getUsername() + "', `password` = '" + user.getPassword() + "', `userTypeCode` = " + user.getUserTypeCode() + " WHERE userAccessId = " + user.getUserAccessId();
            System.out.println(sql);
            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return "Test";
    }

    public static synchronized String delete(int userId) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM`UserAccess` WHERE userAccessId = " + userId;

            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return "Test";
    }

    public static synchronized UserAccess selectUser(int userId) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ResultSet rs = null;
        UserAccess userSelect = new UserAccess();
        System.out.println(userId);

        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM `UserAccess` WHERE userAccessId = " + userId;
            System.out.println(sql);
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                userSelect.setUserAccessId(rs.getInt("userAccessId"));
                userSelect.setUsername(rs.getString("username"));
                userSelect.setPassword(rs.getString("password"));
                userSelect.setUserTypeCode(rs.getInt("userTypeCode"));
                userSelect.setCreatedDateTime(rs.getDate("createdDateTime"));
            }

            System.out.println(userSelect.getUsername());
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return userSelect;
    }
}
