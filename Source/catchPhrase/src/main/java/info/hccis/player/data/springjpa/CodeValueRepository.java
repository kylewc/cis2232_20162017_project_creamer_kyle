/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.data.springjpa;

import info.hccis.player.entity.CodeValue;
import java.util.ArrayList;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author Kyle
 */
@Repository
public interface CodeValueRepository extends CrudRepository<CodeValue, Integer> {

    @Query(value="select englishDescription from codevalue where codeTypeId=?", nativeQuery = true)
    ArrayList<String> findWords(Integer thisId);
    
    @Query(value="select * from codevalue where codeTypeId=?", nativeQuery = true)
    ArrayList<CodeValue> findWordList(Integer thisId);
    
    @Modifying
    @Transactional
    @Query(value="delete from codevalue where codeTypeId=?", nativeQuery = true)
    void deleteByCodeTypeId(Integer thisId);
    
    @Query(value="select * from codevalue where codeTypeId=? LIMIT 1", nativeQuery = true)
    CodeValue findOneByCodeTypeId(Integer thisId);
}


