/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author Kyle
 */
@Entity
@Table(name = "gamedetails")

public class GameDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gameId")
    private Integer gameId;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    @Column(name = "updatedUserId")
    private Integer updatedUserId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "teamIDScore")
    private int teamIDScore;
    @Basic(optional = false)
    @NotNull
    @Column(name = "teamID2Score")
    private int teamID2Score;
    @JoinColumn(name = "categoryID", referencedColumnName = "CodeTypeId")
    @ManyToOne(optional = false)
    private CodeType categoryID;
    @JoinColumn(name = "teamID", referencedColumnName = "teamID")
    @ManyToOne(optional = false)
    private Teams teamID;
    @JoinColumn(name = "teamID2", referencedColumnName = "teamID")
    @ManyToOne(optional = false)
    private Teams teamID2;
    @JoinColumn(name = "userAccessId", referencedColumnName = "userAccessId")
    @ManyToOne(optional = false)
    private UserAccess userAccessId;
    
    @Transient
    private ArrayList<String> wordList;
    
    @Transient
    private String currentTeam;

    public String getCurrentTeam() {
        return currentTeam;
    }

    public void setCurrentTeam(String currentTeam) {
        this.currentTeam = currentTeam;
    }

    public ArrayList<String> getWordList() {
        return wordList;
    }

    public void setWordList(ArrayList<String> wordList) {
        this.wordList = wordList;
    }

    public GameDetails() {
    }

    public GameDetails(Integer gameId) {
        this.gameId = gameId;
    }

    public GameDetails(Integer gameId, int teamIDScore, int teamID2Score) {
        this.gameId = gameId;
        this.teamIDScore = teamIDScore;
        this.teamID2Score = teamID2Score;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public Integer getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Integer updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public int getTeamIDScore() {
        return teamIDScore;
    }

    public void setTeamIDScore(int teamIDScore) {
        this.teamIDScore = teamIDScore;
    }

    public int getTeamID2Score() {
        return teamID2Score;
    }

    public void setTeamID2Score(int teamID2Score) {
        this.teamID2Score = teamID2Score;
    }

    public CodeType getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(CodeType categoryID) {
        this.categoryID = categoryID;
    }

    public Teams getTeamID() {
        return teamID;
    }

    public void setTeamID(Teams teamID) {
        this.teamID = teamID;
    }

    public Teams getTeamID2() {
        return teamID2;
    }

    public void setTeamID2(Teams teamID2) {
        this.teamID2 = teamID2;
    }

    public UserAccess getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(UserAccess userAccessId) {
        this.userAccessId = userAccessId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gameId != null ? gameId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GameDetails)) {
            return false;
        }
        GameDetails other = (GameDetails) object;
        if ((this.gameId == null && other.gameId != null) || (this.gameId != null && !this.gameId.equals(other.gameId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.player.entity.Gamedetails[ gameId=" + gameId + " ]";
    }
    
}
