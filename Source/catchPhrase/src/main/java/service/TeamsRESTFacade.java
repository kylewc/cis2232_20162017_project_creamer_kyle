/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import info.hccis.player.data.springjpa.TeamsRepository;
import info.hccis.player.entity.Teams;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Kyle
 */

@Path("teams")
public class TeamsRESTFacade {

    @Resource
    private final TeamsRepository teamsRepository;

    public TeamsRESTFacade(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.teamsRepository = applicationContext.getBean(TeamsRepository.class);
    }

    @GET
    @Path("/hello")
    public Response hello() {

        System.out.println("in controller for /camper/hello");
        return Response.status(200).entity("hello").build();
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {

        System.out.println("in controller for /teams/all");
        ArrayList<Teams> list = (ArrayList<Teams>)teamsRepository.findAll();
        
        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(list);
        } catch (IOException ex) {
            Logger.getLogger(TeamsRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {

        System.out.println("in controller for /id");
        Teams team = teamsRepository.findOne(id);
        if(team == null){
            return Response.status(204).entity("{}").build();
        }
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(team);
        } catch (IOException ex) {
            Logger.getLogger(TeamsRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }
    
    @GET
    @Path("/teamName/{team}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("team") String id) {

        System.out.println("in controller for /teamName");
        Teams team = teamsRepository.findTeamByName(id);
        if(team == null){
            return Response.status(204).entity("{}").build();
        }
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(team);
        } catch (IOException ex) {
            Logger.getLogger(TeamsRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }
}
