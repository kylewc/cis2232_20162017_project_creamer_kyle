use cis2232_golf_game;

CREATE TABLE IF NOT EXISTS `Game` (
  `gameId` int(5) NOT NULL COMMENT 'This is the primary key for golf games',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

