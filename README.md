# README #

Catch Phrase Game

### What is this repository for? ###

* The Catch Phrase Game -  Load phrases, a random timer with a sounding buzzer.
The Catch Phrase party game was created by Hasbro which now features an electronic device that displays a phrase or word from a built in word list.  The objective of the game is to be the team not guessing when time runs out.  At the beginning of a round, the timer begins for a random amount of time (25-60 seconds) and the page displays a word to the team member along with game details. The team member needs to have their team guess the word.  They can provide hints, but cannot say the word, any letters of the word, any part of the word, or any word that rhymes with the word.  When the timer runs out the opposing team scores a point.  If they team guesses the word, a member of the opposing team now has to have their team guess the new word.  The game ends when a team reaches seven points, or no more words are left.

### How do I get set up? ###

* Clone the repository and open in Netbeans
* Database configuration - run the src/main/resources/ databaseCreation scripts in the order specified.
* Run the application from netbeans using Tomcat web server.

### Contribution guidelines ###

* Developer: Kyle Creamer
* Peer Reviewer: TBD

### Who do I talk to? ###

* Kyle Creamer